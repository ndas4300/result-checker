<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_results', function (Blueprint $table) {
            $table->id();
            $table->integer('student_id');
            $table->integer('semester_id');
            $table->string('grade')->nullable();
            $table->string('fail')->nullable();
            $table->decimal('total_point',3,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_results');
    }
}

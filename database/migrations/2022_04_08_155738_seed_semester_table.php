<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Semester;

class SeedSemesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('semesters', function (Blueprint $table) {
            $data = [
                ['name'=>'1st'],
                ['name'=>'2nd'],
                ['name'=>'3rd'],
                ['name'=>'4th'],
                ['name'=>'5th'],
                ['name'=>'6th'],
                ['name'=>'7th'],
                ['name'=>'8th'],
            ];

            foreach ($data as $d){
                Semester::query()->create($d);
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('semesters', function (Blueprint $table) {
            //
        });
    }
}

<?php

namespace Database\Factories;

use App\Models\Student;
use App\Models\Semester;
use App\Models\Department;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'name' => $this->faker->name,
            'roll' => random_int(10, 499),
            'reg' => random_int(1000, 9999),
            'session' => '17-18',
            'section_id' => 1,
            'shift_id' => 1,
            'department_id' => 2,
            'semester_id' => Semester::all()->random()->id,

        ];
    }
}

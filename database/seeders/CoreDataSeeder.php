<?php

namespace Database\Seeders;

use App\Models\CompanySetting;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CoreDataSeeder extends Seeder
{
    public function run()
    {
        $this->createDefaultSettings();
        $this->createDefaultRolePermissions();
        $this->createDefaultUsers();
    }

    private function createDefaultSettings()
    {
        CompanySetting::create([
            'name'                 => 'Chittagong Polytechnic Institute',
            'mobile1'              => '01xxxxxxxxx',
            'email'                => 'info@gmail.com',
            'paypal_mode'          => 'sandbox',
            'paypal_client_id'     => 'Ad_i93JAPGw-FT4gHEWLZ-t7oflAJmvyi32Q82LG-ARZn0b05wLp-Ov_60tbm_twrs6Dz3o-FKmuylOC',
            'paypal_client_secret' => 'EEkpig_40EKQlg00UQgxydDMHj8GhyqLS48NGe1RQt0cAtrFpqviz_SUNgqiMmBhIaTtiaZuMXi4Myze',
        ]);
    }

    private function createDefaultRolePermissions()
    {

        $permissions = [
            ['name' => 'user.access'],
            ['name' => 'user.create'],
            ['name' => 'user.edit'],
            ['name' => 'user.view'],
            ['name' => 'user.delete'],

            ['name' => 'notice.access'],
            ['name' => 'notice.create'],
            ['name' => 'notice.edit'],
            ['name' => 'notice.view'],
            ['name' => 'notice.delete'],

            ['name' => 'form.access'],
            ['name' => 'form.create'],
            ['name' => 'form.edit'],
            ['name' => 'form.view'],
            ['name' => 'form.delete'],


            ['name' => 'department.access'],
            ['name' => 'department.create'],
            ['name' => 'department.edit'],
            ['name' => 'department.view'],
            ['name' => 'department.delete'],


            ['name' => 'subject.access'],
            ['name' => 'subject.create'],
            ['name' => 'subject.edit'],
            ['name' => 'subject.view'],
            ['name' => 'subject.delete'],


            ['name' => 'student.access'],
            ['name' => 'student.create'],
            ['name' => 'student.edit'],
            ['name' => 'student.view'],
            ['name' => 'student.delete'],


            ['name' => 'result.access'],
            ['name' => 'result.create'],
            ['name' => 'result.edit'],
            ['name' => 'result.view'],
            ['name' => 'result.delete'],



            ['name' => 'contact_message.access'],
            ['name' => 'contact_message.delete'],

            ['name' => 'profile.access'],
            ['name' => 'profile.edit'],

            ['name' => 'settings.access'],
            ['name' => 'settings.edit'],

            ['name' => 'role_permission.access'],
            ['name' => 'role_permission.create'],
            ['name' => 'role_permission.edit'],
            ['name' => 'role_permission.view'],
            ['name' => 'role_permission.delete'],
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission["name"]
            ]);
        };

        //get all permissions via Gate::before rule; see AuthServiceProvider
        Role::create([
            'name' => 'Admin'
        ]);

        $userPermissions = [
            'profile.access',
            'profile.edit',
        ];

        Role::create([
            'name' => 'User'
        ])->givePermissionTo($userPermissions);
    }

    private function createDefaultUsers()
    {
        User::create([
            'name'              => 'super admin',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('123123123'),
            'remember_token'    => Str::random(10),
        ])->assignRole('Admin');
    }
}

<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  public function run(): void
  {
    $this->call(CoreDataSeeder::class);
    $this->call(DemoDataSeeder::class);
//     Student::factory(100)->create();
  }
}

<?php

namespace App\Observers;

use App\Models\MainResult;
use App\Models\Result;
use App\Models\Student;
use App\Models\Subject;

class ResultObserver
{
    /**
     * Handle the Result "created" event.
     *
     * @param  \App\Models\Result  $result
     * @return void
     */
    public function created(Result $result)
    {
        $subjectCount=Subject::whereHas( 'departments',function ($q) use ($result){
            return $q->where('department_id',$result->department_id);
        })->whereHas( 'semesters',function ($q) use ($result){
            return $q->where('semester_id',$result->semester_id);
        })->count();
        $failed=Result::where('student_id',$result->student_id)
            ->where('semester_id',$result->semester_id)->where('grade','F')->exists();
        if (!$failed){
            $datas=Result::where('student_id',$result->student_id)
                ->where('semester_id',$result->semester_id)->get()
                ->unique('subject_id');

            $resultCount=count($datas);

            if ( $subjectCount == $resultCount){
                $total_point='';
                foreach ($datas as $data){
                    $total_point= (float)($data->total_point) + (float)($total_point);
                }
                if ($failed){
                    $point= 0.00;
                }else{
                    $point= $total_point / $subjectCount;
                }
                if ($point == 4.00 ) {
                    $grade= 'A+';

                }elseif ($point >= 3.75 ) {
                    $grade= 'A';

                }elseif ($point >= 3.50 ) {
                    $grade= 'A-';

                }elseif ($point >= 3.75 ) {
                    $grade= 'B+';

                }elseif ($point >= 3.50) {
                    $grade= 'B';

                }elseif ($point >= 3.25 ) {
                    $grade= 'B-';

                }elseif ($point >= 3.00) {
                    $grade= 'C+';

                }elseif ($point >= 2.75 ) {
                    $grade= 'C';

                }elseif ($point >= 2.50) {
                    $grade= 'D';

                }
                else{
                    $grade= 'F';
                }

                $f=[];
                if ($result->grade == 'F'){
                    array_push($f,$result->subject->subject_code);
                }
                $d=collect($f)->implode(',');
                $mainResultExists=MainResult::where('student_id',$result->student_id)
                    ->where('semester_id',$result->semester_id)->exists();
                $mainResult=MainResult::where('student_id',$result->student_id)
                    ->where('semester_id',$result->semester_id)->first();
                if ($mainResultExists){
                    $mainResult->update([
                        'student_id'        => $result->student_id,
                        'semester_id'       => $result->semester_id,
                        'grade'             => $grade,
                        'fail'              => $d,
                        'total_point'       => $point,
                    ]);
                }else{
                    MainResult::create([
                        'student_id'        => $result->student_id,
                        'semester_id'       => $result->semester_id,
                        'grade'             => $grade,
                        'fail'              => $d,
                        'total_point'       => $point,
                    ]);
                }
            }
        }


    }

    /**
     * Handle the Result "updated" event.
     *
     * @param  \App\Models\Result  $result
     * @return void
     */
    public function updated(Result $result)
    {

        $subjectCount=Subject::whereHas( 'departments',function ($q) use ($result){
            return $q->where('department_id',$result->department_id);
        })->whereHas( 'semesters',function ($q) use ($result){
            return $q->where('semester_id',$result->semester_id);
        })->count();

        $data=Result::where('student_id',$result->student_id)
            ->where('semester_id',$result->semester_id)->get()
            ->unique('subject_id');
        $resultCount=count($data);

        if ( $subjectCount == $resultCount){
            $total_point='';
            foreach ($data as $data){
                $total_point= (float)($data->total_point) + (float)($total_point);
            }
            $point= $total_point / $subjectCount;

            if ($total_point == 4.00 ) {
                $grade= 'A+';

            }elseif ($total_point >= 3.75 ) {
                $grade= 'A';

            }elseif ($total_point >= 3.50 ) {
                $grade= 'A-';

            }elseif ($total_point >= 3.75 ) {
                $grade= 'B+';

            }elseif ($total_point >= 3.50) {
                $grade= 'B';

            }elseif ($total_point >= 3.25 ) {
                $grade= 'B-';

            }elseif ($total_point >= 3.00) {
                $grade= 'C+';

            }elseif ($total_point >= 2.75 ) {
                $grade= 'C';

            }elseif ($total_point >= 2.50) {
                $grade= 'D';

            }
            else{
                $grade= 'F';
            }

            $f=[];
            if ($result->grade == 'F'){
                array_push($f,$result->subject->subject_code);
            }
            $d=collect($f)->implode(',');

            $mainResult=MainResult::where('student_id',$result->student_id)
                ->where('semester_id',$result->semester_id)->first();
            $mainResult->update([
                'student_id'        => $result->student_id,
                'semester_id'       => $result->semester_id,
                'grade'             => $grade,
                'fail'              => $d,
                'total_point'       => $point,
            ]);

        }

    }

    /**
     * Handle the Result "deleted" event.
     *
     * @param  \App\Models\Result  $result
     * @return void
     */
    public function deleted(Result $result)
    {
        $mainResult=MainResult::where('student_id',$result->student_id)
            ->where('semester_id',$result->semester_id)->first();
        $mainResult->delete();
    }

    /**
     * Handle the Result "restored" event.
     *
     * @param  \App\Models\Result  $result
     * @return void
     */
    public function restored(Result $result)
    {
        //
    }

    /**
     * Handle the Result "force deleted" event.
     *
     * @param  \App\Models\Result  $result
     * @return void
     */
    public function forceDeleted(Result $result)
    {
        //
    }
}

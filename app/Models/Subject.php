<?php

namespace App\Models;

use App\Models\Result;
use App\Models\Semester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subject extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'subject_code',
        'credit',
        'tc',
        'pc',
        'tf',
        'pf',
        'total_marks',
    ];

    public function departments()
    {
        return $this->belongsToMany(Department::class);
    }

    public function semesters()
    {
        return $this->belongsToMany(Semester::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }


}

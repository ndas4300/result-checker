<?php

namespace App\Models;

use App\Models\Result;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Semester extends Model
{
    use HasFactory;
    protected $fillable = [
        'name'
    ];

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function mainResults()
    {
        return $this->hasMany(MainResult::class);
    }
}

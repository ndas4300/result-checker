<?php

namespace App\Models;

use App\Models\Shift;
use App\Models\Result;
use App\Models\Subject;
use App\Models\Section;
use App\Models\Semester;
use App\Models\Department;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'roll',
        'reg',
        'session',
        'section_id',
        'shift_id',
        'semester_id',
        'department_id'
    ];

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }
    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }
    public function section()
    {
        return $this->belongsTo(Section::class);
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function mainResults()
    {
        return $this->hasMany(MainResult::class);
    }

    public function semesterSubject(){
        return $this->hasManyThrough(Subject::class,Semester::class);
    }
}

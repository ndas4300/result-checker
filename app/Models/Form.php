<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'roll',
        'reg_no',
        'section',
        'phone',
        'size',
        'religion',
        'status',
    ];
}

<?php

namespace App\Models;

use App\Models\Student;
use App\Models\Subject;
use App\Models\Semester;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Result extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'semester_id',
        'department_id',
        'subject_id',
        'tc',
        'pc',
        'tf',
        'pf',
        'total_marks',
        'grade',
        'fail',
        'total_point',
    ];

    protected $cast=['fail'=>'array'];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}

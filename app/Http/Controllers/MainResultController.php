<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\MainResult;
use App\Models\Semester;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;

class MainResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->checkPermission('result.access');

        $mainResults = MainResult::with(['student','semester'])->paginate($this->itemPerPage);
        $this->putSL($mainResults);
        return view('dashboard.mainResult.index', compact('mainResults'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $semesters      =Semester::get();
        $departments    =Department::get();
        return view('dashboard.mainResult.search',compact('semesters','departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function result(Request $request)
    {
        $student=Student::where('department_id',$request->department_id)
            ->where('roll',$request->roll)
            ->where('reg',$request->reg)
            ->first();

        $mainResult= MainResult::where('semester_id',$request->semester_id)
            ->where('student_id',$student->id)
            ->first();
        return view('dashboard.mainResult.result', compact('mainResult'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainResult  $mainResult
     * @return \Illuminate\Http\Response
     */
    public function show(MainResult $mainResult)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainResult  $mainResult
     * @return \Illuminate\Http\Response
     */
    public function edit(MainResult $mainResult)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainResult  $mainResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainResult $mainResult)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainResult  $mainResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainResult $mainResult)
    {
        //
    }
}

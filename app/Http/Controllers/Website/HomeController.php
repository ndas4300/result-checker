<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\MainResult;
use App\Models\Semester;
use App\Models\Student;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $semesters      =Semester::get();
        $departments    =Department::get();
        return view('website.index',compact('semesters','departments'));
    }

    public function result(Request $request)
    {
//        dd($request->all());
        $student=Student::where('department_id',$request->department_id)
            ->where('roll',$request->roll)
            ->where('reg',$request->reg)
            ->first();

        $mainResult= MainResult::where('semester_id',$request->semester_id)
            ->where('student_id',$student->id)
            ->first();
        return view('website.result', compact('mainResult','student'));
    }
}

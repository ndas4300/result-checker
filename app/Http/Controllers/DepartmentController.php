<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Subject;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    public function index()
    {
        $this->checkPermission('department.access');

        $departments = Department::paginate($this->itemPerPage);
        $this->putSL($departments);
        return view('dashboard.department.index', compact('departments'));
    }

    public function create()
    {
        $this->checkPermission('department.create');

        return view('dashboard.department.create');
    }

    public function store(Request $request)
    {
        $this->checkPermission('department.create');

        $validated = $request->validate([
            'name'           => ['required', 'string', 'max:255'],
        ]);

        $department=Department::create($validated);


        return redirect()->route('department.index')->with('success', 'Department Created Successfully.');
    }

    public function edit(Department $department)
    {
        $this->checkPermission('department.edit');

        return view('dashboard.department.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
        $this->checkPermission('department.edit');

        $validated = $request->validate([
            'name'           => ['required', 'string', 'max:255'],
        ]);

        $department->update($validated);

        $department->subjects()->sync($request->subject_id);

        return redirect()->route('department.index')->with('success', 'Department updated successfully.');
    }

    public function destroy(Department $department)
    {
        $this->checkPermission('department.delete');

        $department->delete();
        return back()->with('success', 'Department deleted successfully.');
    }
}

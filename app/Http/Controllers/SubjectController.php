<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Subject;
use App\Models\Semester;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $this->checkPermission('subject.access');

        $subjects = Subject::with(['semesters','departments'])->paginate($this->itemPerPage);
        // dd($subjects);
        $this->putSL($subjects);
        return view('dashboard.subject.index', compact('subjects'));
    }

    public function create()
    {
        $this->checkPermission('subject.create');

        $semesters=Semester::get();
        $departments=Department::get();

        return view('dashboard.subject.create',compact('semesters','departments'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->checkPermission('subject.create');

        $validated = $request->validate([
            'name'              => ['required', 'string', 'max:255'],
            'subject_code'      => ['required', 'integer'],
            'credit'            => ['required', 'integer'],
            'tc'                => ['nullable', 'integer'],
            'pc'                => ['nullable', 'integer'],
            'tf'                => ['nullable', 'integer'],
            'pf'                => ['nullable', 'integer'],
        ]);

        $validated['total_marks'] = $request->tc+$request->pc+$request->tf+$request->pf ;

        $subject=Subject::create($validated);
        $subject->departments()->attach($request->department_id);
        $subject->semesters()->attach($request->semester_id);


        return redirect()->route('subject.index')->with('success', 'Subject Created Successfully.');
    }

    public function edit(Subject $subject)
    {
        $this->checkPermission('subject.edit');

        $semesters=Semester::get();
        $departments=Department::get();

        return view('dashboard.subject.edit', compact('subject','semesters','departments'));
    }

    public function update(Request $request, Subject $subject)
    {
        $this->checkPermission('subject.edit');

        $validated = $request->validate([
            'name'              => ['required', 'string', 'max:255'],
            'subject_code'      => ['required', 'integer'],
            'credit'            => ['required', 'integer'],
            'tc'                => ['nullable', 'integer'],
            'pc'                => ['nullable', 'integer'],
            'tf'                => ['nullable', 'integer'],
            'pf'                => ['nullable', 'integer'],
        ]);

        $validated['total_marks'] = $request->tc+$request->pc+$request->tf+$request->pf ;

        $subject->update($validated);
        $subject->departments()->sync($request->department_id);
        $subject->semesters()->sync($request->semester_id);


        return redirect()->route('subject.index')->with('success', 'Subject updated successfully.');
    }

    public function destroy(Subject $subject)
    {
        $this->checkPermission('subject.delete');

        $subject->delete();
        return back()->with('success', 'Subject deleted successfully.');
    }
}

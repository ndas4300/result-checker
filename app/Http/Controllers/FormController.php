<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index()
    {
        $this->checkPermission('form.access');

        $forms = Form::paginate($this->itemPerPage);
        $this->putSL($forms);
        return view('dashboard.form.index', compact('forms'));
    }

    public function create()
    {
        $this->checkPermission('form.create');

        return view('dashboard.form.create');
    }

    public function store(Request $request)
    {
        $this->checkPermission('form.create');

        $validated = $request->validate([
            'name'        => ['required', 'string', 'max:255'],
            'roll'        => ['required', 'string'],
            'reg_no'      => ['required', 'string'],
            'section'     => ['required', 'string'],
            'phone'       => ['required', 'string'],
            'size'        => ['required', 'string'],
            'religion'    => ['required', 'string'],
            'status'      => ['required', 'boolean'],
        ]);

        Form::create($validated);

        return redirect()->route('form.index')->with('success', 'Form Created Successfully.');
    }

    public function edit(Form $form)
    {
        $this->checkPermission('form.edit');

        return view('dashboard.form.edit', compact('form'));
    }

    public function update(Request $request, Form $form)
    {
        $this->checkPermission('form.edit');

        $validated = $request->validate([
            'name'        => ['required', 'string', 'max:255'],
            'roll'        => ['required', 'string'],
            'reg_no'      => ['required', 'string'],
            'section'     => ['required', 'string'],
            'phone'       => ['required', 'string'],
            'size'        => ['required', 'string'],
            'religion'    => ['required', 'string'],
            'status'      => ['required', 'boolean'],
        ]);

        $form->update($validated);

        return redirect()->route('form.index')->with('success', 'Form updated successfully.');
    }

    public function destroy(Form $form)
    {
        $this->checkPermission('form.delete');

        $form->delete();
        return back()->with('success', 'Form deleted successfully.');
    }
}

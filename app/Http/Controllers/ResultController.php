<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Result;
use App\Models\Subject;
use App\Models\Semester;
use App\Models\Shift;
use App\Models\Student;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function index()
    {
        $this->checkPermission('result.access');

        $results = Result::with(['student','semester','subject'])->paginate($this->itemPerPage);
        $this->putSL($results);
        return view('dashboard.result.index', compact('results'));
    }

    public function create()
    {
        $this->checkPermission('result.create');

        $students       =Student::get();
        $semesters      =Semester::get();
        $subjects       =Subject::get();
        $departments    =Department::get();

        return view('dashboard.result.create',compact('semesters','subjects','students','departments'));
    }

    public function store(Request $request)
    {
        $this->checkPermission('result.create');

        $validated = $request->validate([
            'student_id'        => ['required', 'integer'],
            'semester_id'       => ['required', 'integer'],
            'department_id'     => ['required', 'integer'],
            'subject_id'        => ['required', 'integer'],
            'tc'                => ['nullable', 'string'],
            'pc'                => ['nullable', 'string'],
            'tf'                => ['nullable', 'integer'],
            'pf'                => ['nullable', 'integer'],
        ]);
        $total_marks = $request->tc+$request->pc+$request->tf+$request->pf ;
        $validated['total_marks'] = $total_marks ;

        $subject=Subject::where('id',$request->subject_id)->first();
        $tc= $subject->tc * 0.4;
        $pc= $subject->pc * 0.4;
        $tf= $subject->tf * 0.4;
        $pf= $subject->pf * 0.4;

        if ($request->tc < $tc || $request->pc < $pc || $request->tf < $tf || $request->pf < $pf  ) {
            $validated['grade']= 'F';
            $validated['total_point']= 0.00;
        }else {
            if ($total_marks >= ($subject->total_marks * 0.8) ) {
                $validated['grade']= 'A+';
                $validated['total_point']= 4.00;
            }elseif ($total_marks >= ($subject->total_marks * 0.75) ) {
                $validated['grade']= 'A';
                $validated['total_point']= 3.75;
            }elseif ($total_marks >= ($subject->total_marks * 0.7) ) {
                $validated['grade']= 'A-';
                $validated['total_point']= 3.50;
            }elseif ($total_marks >= ($subject->total_marks * 0.65) ) {
                $validated['grade']= 'B+';
                $validated['total_point']= 3.25;
            }elseif ($total_marks >= ($subject->total_marks * 0.6) ) {
                $validated['grade']= 'B';
                $validated['total_point']= 3.00;
            }elseif ($total_marks >= ($subject->total_marks * 0.55) ) {
                $validated['grade']= 'B-';
                $validated['total_point']= 2.75;
            }elseif ($total_marks >= ($subject->total_marks * 0.5) ) {
                $validated['grade']= 'C+';
                $validated['total_point']= 2.50;
            }elseif ($total_marks >= ($subject->total_marks * 0.45) ) {
                $validated['grade']= 'C';
                $validated['total_point']= 2.25;
            }elseif ($total_marks >= ($subject->total_marks * 0.4) ) {
                $validated['grade']= 'D';
                $validated['total_point']= 2.00;
            }
        }

        $f=[];
        if ($request->tc < $tc){
            array_push($f,'tc');
        }
        if($request->pc < $pc){
        array_push($f,'tf');
        }
        if($request->tf < $tf){
        array_push($f,'pc');
        }
        if($request->pf < $pf){
        array_push($f,'pf');
        }
        $d=collect($f)->implode(',');

        $validated['fail']= $d;


        Result::create($validated);

        return redirect()->route('result.index')->with('success', 'Result Created Successfully.');
    }

    public function edit(Result $result)
    {
        $this->checkPermission('result.edit');
        $students   =Student::get();
        $semesters  =Semester::get();
        $subjects   =Subject::get();

        return view('dashboard.result.edit', compact('result', 'subjects','semesters','students'));
    }

    public function update(Request $request, Result $result)
    {
        $this->checkPermission('result.edit');

        $validated = $request->validate([
            'student_id'        => ['required', 'integer'],
            'semester_id'       => ['required', 'integer'],
            'department_id'     => ['required', 'integer'],
            'subject_id'        => ['required', 'integer'],
            'tc'                => ['nullable', 'string'],
            'pc'                => ['nullable', 'string'],
            'tf'                => ['nullable', 'integer'],
            'pf'                => ['nullable', 'integer'],
        ]);
        $total_marks = $request->tc+$request->pc+$request->tf+$request->pf ;
        $validated['total_marks'] = $total_marks ;

        $subject=Subject::where('id',$request->subject_id)->first();
        $tc= $subject->tc * 0.4;
        $pc= $subject->pc * 0.4;
        $tf= $subject->tf * 0.4;
        $pf= $subject->pf * 0.4;

        if ($request->tc < $tc || $request->pc < $pc || $request->tf < $tf || $request->pf < $pf  ) {
            $validated['grade']= 'F';
            $validated['total_point']= 0.00;
        }
        else {
//            dd('not');
            if ($total_marks >= ($subject->total_marks * 0.8) ) {
                $validated['grade']= 'A+';
                $validated['total_point']= 4.00;
            }elseif ($total_marks >= ($subject->total_marks * 0.75) ) {
                $validated['grade']= 'A';
                $validated['total_point']= 3.75;
            }elseif ($total_marks >= ($subject->total_marks * 0.7) ) {
                $validated['grade']= 'A-';
                $validated['total_point']= 3.50;
            }elseif ($total_marks >= ($subject->total_marks * 0.65) ) {
                $validated['grade']= 'B+';
                $validated['total_point']= 3.25;
            }elseif ($total_marks >= ($subject->total_marks * 0.6) ) {
                $validated['grade']= 'B';
                $validated['total_point']= 3.00;
            }elseif ($total_marks >= ($subject->total_marks * 0.55) ) {
                $validated['grade']= 'B-';
                $validated['total_point']= 2.75;
            }elseif ($total_marks >= ($subject->total_marks * 0.5) ) {
                $validated['grade']= 'C+';
                $validated['total_point']= 2.50;
            }elseif ($total_marks >= ($subject->total_marks * 0.45) ) {
                $validated['grade']= 'C';
                $validated['total_point']= 2.25;
            }elseif ($total_marks >= ($subject->total_marks * 0.4) ) {
                $validated['grade']= 'D';
                $validated['total_point']= 2.00;
            }
        }

        $f=[];
        if ($request->tc < $tc){
            array_push($f,'tc');
        }
        if($request->pc < $pc){
        array_push($f,'tf');
        }
        if($request->tf < $tf){
        array_push($f,'pc');
        }
        if($request->pf < $pf){
        array_push($f,'pf');
        }
        $d=collect($f)->implode(',');

        $validated['fail']= $d;

        $result->update($validated);

        return redirect()->route('result.index')->with('success', 'Result updated successfully.');
    }

    public function destroy(Result $result)
    {
        $this->checkPermission('result.delete');

        $result->delete();
        return back()->with('success', 'Result deleted successfully.');
    }


    public function particular()
    {
        $this->checkPermission('result.access');

        $semesters      =Semester::get();
        $subjects       =Subject::get();
        $departments    =Department::get();
        $results        =Result::with(['student','semester','subject'])->paginate($this->itemPerPage);
        $this->putSL($results);
        return view('dashboard.result.particular', compact('results','semesters','subjects','departments'));
    }


    public function particularData(Request $request)
    {
        $this->checkPermission('result.access');
        // dd($request->semester_id);

        $semesters      =Semester::get();
        $subjects       =Subject::get();
        $departments    =Department::get();
        $subject        =$request->subject_id;
        $semester       =$request->semester_id;
        $department     =$request->department_id;
        $students       =Student::where('semester_id',$request->semester_id)->where('department_id',$request->department_id)->paginate($this->itemPerPage);
        // dd($students);
        $this->putSL($students);
        return view('dashboard.result.particular', compact('students','semesters','subjects','departments','subject','semester','department'));
    }


    public function dataStore(Request $request)
    {
        $this->checkPermission('result.access');
        dd($request->all());
        return view('dashboard.result.particular', compact('students','semesters','subjects','departments'));
    }
}

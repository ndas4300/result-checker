<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Department;
use App\Models\MainResult;
use App\Models\Semester;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function departments()
    {
        $departments=Department::all();
        return $departments;
    }

    public function results(Request $request)
    {
//        dd($request->all());
        $student=Student::where('department_id',$request->department_id)
            ->where('roll',$request->roll)
            ->where('reg',$request->reg)
            ->first();

        $mainResult= MainResult::where('semester_id',$request->semester_id)
            ->where('student_id',$student->id)->with('student')
            ->first();

//        dd($mainResult);
        return $mainResult;
    }
}

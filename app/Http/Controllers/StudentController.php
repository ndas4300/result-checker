<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Section;
use App\Models\Semester;
use App\Models\Shift;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $this->checkPermission('student.access');

        $students = Student::with(['department','semester','section','shift'])->paginate($this->itemPerPage);
        $this->putSL($students);
        return view('dashboard.student.index', compact('students'));
    }

    public function create()
    {
        $this->checkPermission('student.create');

        $semesters=Semester::get();
        $sections=Section::get();
        $shifts=Shift::get();
        $departments=Department::get();

        return view('dashboard.student.create',compact('semesters','sections','shifts','departments'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->checkPermission('student.create');

        $validated = $request->validate([
            'name'              => ['required', 'string', 'max:255'],
            'roll'              => ['required', 'integer'],
            'reg'               => ['required', 'integer'],
            'session'           => ['required', 'string'],
            'section_id'        => ['required', 'integer'],
            'shift_id'          => ['required', 'integer'],
            'semester_id'       => ['required', 'integer'],
            'department_id'     => ['required', 'integer'],
        ]);

        Student::create($validated);

        return redirect()->route('student.index')->with('success', 'Student Created Successfully.');
    }

    public function edit(Student $student)
    {
        $this->checkPermission('student.edit');
        // dd($student);

        $semesters=Semester::get();
        $sections=Section::get();
        $shifts=Shift::get();
        $departments=Department::get();
        // dd($students);


        return view('dashboard.student.edit', compact('student','semesters','sections','shifts','departments'));
    }

    public function update(Request $request, Student $student)
    {
        $this->checkPermission('student.edit');

        $validated = $request->validate([
            'name'              => ['required', 'string', 'max:255'],
            'roll'              => ['required', 'integer'],
            'reg'               => ['required', 'integer'],
            'session'           => ['required', 'string'],
            'section_id'        => ['required', 'integer'],
            'shift_id'          => ['required', 'integer'],
            'semester_id'       => ['required', 'integer'],
            'department_id'     => ['required', 'integer'],
        ]);

        $student->update($validated);

        return redirect()->route('student.index')->with('success', 'Student updated successfully.');
    }

    public function destroy(Student $student)
    {
        $this->checkPermission('student.delete');

        $student->delete();
        return back()->with('success', 'Student deleted successfully.');
    }
}

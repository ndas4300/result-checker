<?php

use App\Http\Controllers\MainResultController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\FormController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\Admin\NoticeController;
use App\Http\Controllers\Website\HomeController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Website\ContactMessageController;
use Spatie\MediaLibrary\MediaCollections\Models\Media as MediaAlias;

// cache clear
Route::get('reboot', function () {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    dd('Done');
});
Route::get('storageLink', function () {
    Artisan::call('storage:link');
    dd('Done');
});
Route::get('migrate', function () {
    Artisan::call('migrate');
    dd('Done');
});

Route::get('migrate-seed', function () {
    Artisan::call('migrate:fresh --seed');
    dd('Done');
});

Route:: middleware('web')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::post('result/search',[HomeController::class,'result'])->name('result.search');
});

Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
    Route::view('/', 'dashboard.dashboard')->name('dashboard');
    Route::resource('user', UserController::class);
    Route::resource('faq', FaqController::class)->except('index', 'show');
    Route::resource('contact-message', ContactMessageController::class)->except(['show', 'create', 'edit', 'update']);
    Route::resource('notice', NoticeController::class)->except('show');
    Route::resource('form', FormController::class)->except('show');
    Route::resource('student', StudentController::class)->except('show');
    Route::resource('subject', SubjectController::class)->except('show');
    Route::resource('department', DepartmentController::class)->except('show');
    Route::resource('result', ResultController::class)->except('show');
    Route::get('result/particular', [ResultController::class, 'particular'])->name('result.particular');
    Route::post('result/particularData',[ResultController::class,'particularData'])->name('result.particularData');
    Route::post('result/dataStore',[ResultController::class,'dataStore'])->name('result.dataStore');

//    Main Results
    Route::get('mainResult',[MainResultController::class,'index'])->name('mainResult.index');
    Route::get('mainResult/search',[MainResultController::class,'search'])->name('mainResult.search');
    Route::post('mainResult/result',[MainResultController::class,'result'])->name('mainResult.result');

    Route::get('change_password', [UserController::class, 'change_password'])->name('change_password');
    Route::get('settings/company_settings', [SettingController::class, 'editCompanySetting'])->name('company.edit');
    Route::post('settings/company_setting', [SettingController::class, 'updateCompanySetting'])->name('company.update');

    Route::delete('remove-media/{media}', function (MediaAlias $media) {
        $media->delete();
        return back()->with('success', 'Media successfully deleted.');
    })->name('remove-media');

    // Role Permission
    Route::resource('developer/permission', PermissionController::class)->only('index', 'store');
    Route::get('role/assign', [RoleController::class, 'roleAssign'])->name('role.assign');
    Route::post('role/assign', [RoleController::class, 'storeAssign'])->name('store.assign');
    Route::resource('role', RoleController::class);
});

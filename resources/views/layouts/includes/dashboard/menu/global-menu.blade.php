{{-- @can('contact_message.access')
    <li class="menu-item {{ request()->is('dashboard/contact-message*') ? 'menu-item-active' : null }}" aria-haspopup="true">
        <a href="{{ route('contact-message.index') }}" class="menu-link">
            <span class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z"
                            fill="#000000" />
                        <path
                            d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z"
                            fill="#000000" opacity="0.3" />
                    </g>
                </svg>
            </span>
            <span class="menu-text">Contact Messages</span>
        </a>
    </li>
@endcan

@can('notice.access')
    <li class="menu-item {{ request()->is('dashboard/notice*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Notices</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Notices</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/notice/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('notice.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Notice</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/notice') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('notice.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Notice</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan --}}

@can('department.access')
    <li class="menu-item {{ request()->is('dashboard/department*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Departments</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Departments</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/department/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('department.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Department</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/department') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('department.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Department</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan

@can('subject.access')
    <li class="menu-item {{ request()->is('dashboard/subject*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Subjects</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Subjects</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/subject/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('subject.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Subject</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/subject') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('subject.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Subject</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan

@can('student.access')
    <li class="menu-item {{ request()->is('dashboard/student*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Students</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Students</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/student/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('student.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Student</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/student') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('student.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Student</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan


@can('result.access')
    <li class="menu-item {{ request()->is('dashboard/result*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Results</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Results</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/result/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('result.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Result</span>
                    </a>
                </li>

{{--                <li class="menu-item {{ request()->is('dashboard/result/particular') ? 'menu-item-active' : null }}" aria-haspopup="true">--}}
{{--                    <a href="{{ route('result.particular') }}" class="menu-link">--}}
{{--                        <i class="menu-bullet menu-bullet-line">--}}
{{--                            <span></span>--}}
{{--                        </i>--}}
{{--                        <span class="menu-text">Add Particular Result</span>--}}
{{--                    </a>--}}
{{--                </li>--}}

                <li class="menu-item {{ request()->is('dashboard/result') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('result.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Result</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan
@can('mainResult.access')
    <li class="menu-item {{ request()->is('dashboard/mainResult*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Main Results</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Main Results</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/mainResult/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('mainResult.search') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Search Result</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/mainResult') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('mainResult.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Main Result</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan

{{-- @can('form.access')
    <li class="menu-item {{ request()->is('dashboard/form*') ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <div class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z"
                            fill="#000000" />
                        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5" />
                    </g>
                </svg>
            </div>
            <span class="menu-text">Forms</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                <li class="menu-item menu-item-parent" aria-haspopup="true">
                    <span class="menu-link"><span class="menu-text">Forms</span></span>
                </li>

                <li class="menu-item {{ request()->is('dashboard/form/create') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('form.create') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">Add Form</span>
                    </a>
                </li>

                <li class="menu-item {{ request()->is('dashboard/form') ? 'menu-item-active' : null }}" aria-haspopup="true">
                    <a href="{{ route('form.index') }}" class="menu-link">
                        <i class="menu-bullet menu-bullet-line">
                            <span></span>
                        </i>
                        <span class="menu-text">All Form</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>
@endcan --}}

<li class="menu-section">
    <h4 class="menu-text">Settings</h4>
    <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
</li>

{{-- @can('profile.access')
    <li class="menu-item {{ request()->is(['dashboard/user*', 'dashboard/change_password']) ? 'menu-item-active' : null }}" aria-haspopup="true">
        <a href="{{ route('user.edit', auth()->id()) }}" class="menu-link">
            <span class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero"
                            opacity="0.3" />
                        <path
                            d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                            fill="#000000" fill-rule="nonzero" />
                    </g>
                </svg>
            </span>
            <span class="menu-text">Profile</span>
        </a>
    </li>
@endcan --}}

@canany(['settings.access', 'role_permission.access'])
    <li class="menu-item {{ request()->is(['dashboard/settings*', 'dashboard/role*']) ? 'menu-item-active menu-item-open' : null }}" aria-haspopup="true" data-menu-toggle="hover">
        <a href="javascript:" class="menu-link menu-toggle">
            <span class="svg-icon menu-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path
                            d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z"
                            fill="#000000" />
                        <path
                            d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z"
                            fill="#000000" opacity="0.3" />
                    </g>
                </svg>
            </span>
            <span class="menu-text">Settings</span>
            <i class="menu-arrow"></i>
        </a>
        <div class="menu-submenu">
            <i class="menu-arrow"></i>
            <ul class="menu-subnav">
                {{-- @can('settings.access')
                    <li class="menu-item {{ request()->is('dashboard/settings/company_settings*') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('company.edit') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                                <span></span>
                            </i>
                            <span class="menu-text">Company Setting</span>
                        </a>
                    </li>
                @endcan --}}
                @can('role_permission.access')
                    <li class="menu-item {{ request()->is('dashboard/role*') ? 'menu-item-active' : null }}" aria-haspopup="true">
                        <a href="{{ route('role.assign') }}" class="menu-link">
                            <i class="menu-bullet menu-bullet-line">
                                <span></span>
                            </i>
                            <span class="menu-text">Role permission</span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </li>
@endcanany

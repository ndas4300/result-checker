<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Chittagong Polytechnic Institute</title>
  <link rel="stylesheet" href="{{asset('assets/result/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/result/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/result/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/result/css/responsive.css')}}">
</head>
<body>

<div class="header_part">
  <img src="{{asset('assets/result/image/logos2.png')}}" class="img-fluid" alt="">
</div>


@yield('content')

<footer class="footer">
  <h2>©2022 Computer Department, All rights reserved.</h2>
</footer>



<script src="{{asset('assets/result/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/result/js/jquery-1.12.4.min.js')}}"></script>
</body>
</html>


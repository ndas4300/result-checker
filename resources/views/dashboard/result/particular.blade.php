@extends('layouts.dashboard')

@section('content')


<div class="row">
  <div class="card card-body">
    <form class="form" id="kt_form" method="post" action="{{ route('result.particularData') }}">
      @csrf
    <div class="col-lg-3">
      <div class="form-group">
        <select class="form-control select2" id="department_id" name="department_id">
          @foreach ($departments as $department)
          <option value="{{ $department->id }}">
            {{ $department->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>


    <div class="col-lg-3">
      <div class="form-group">
        <select class="form-control select2" id="semester_id" name="semester_id">
          @foreach ($semesters as $semester)
          <option value="{{ $semester->id }}">
            {{ $semester->name }}
          </option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <select class="form-control select2" id="subject_id" name="subject_id">
          @foreach ($subjects as $subject)
          <option value="{{ $subject->id }}">
            {{ $subject->name }} ({{ $subject->subject_code }})
          </option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="form-group">
        <button type="submit"  class="btn btn-primary font-weight-bolder">
          <span class="svg-icon svg-icon-md">
            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <rect x="0" y="0" width="24" height="24"></rect>
                <circle fill="#000000" cx="9" cy="15" r="6"></circle>
                <path
                  d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                  fill="#000000" opacity="0.3"></path>
              </g>
            </svg>
          </span>
          Search Students
        </button>
      </div>
    </div>
    </form>

  </div>

</div>

<br>

<div class="card card-custom">
  <form class="form" id="kt_form" method="post" action="{{ route('result.dataStore') }}">
    @csrf
  <div class="card-header flex-wrap border-0 pt-6 pb-0">
    <div class="card-title">
      <h3 class="card-label">Student List</h3>
    </div>
    <div class="card-toolbar">
      @can('web_content.create')
      <button type="submit" class="btn btn-primary font-weight-bolder">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path
                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>
        Add Result
      </button>
      @endcan
    </div>
  </div>
  <div class="card-body">
    @if (isset($students))

    <div class="table-responsive">
      <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
        <thead>
          <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Roll</th>
            <th>Tc Marks</th>
            <th>Pc Marks</th>
            <th>Tf Marks</th>
            <th>Pf Marks</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($students as $student)
          <tr>
            <td>
              {{ $student->sl }}
            </td>
            <td>
              {{ $student->name }}
              <input type="hidden" name="student_id" value="{{$student->id}}">
              <input type="hidden" name="subject_id" value="{{$subject}}">
              <input type="hidden" name="semester_id" value="{{$semester}}">
              <input type="hidden" name="department_id" value="{{$department}}">
            </td>
            <td>
              {{ $student->roll }}
            </td>
            <td>
              <input type="number" name="tc"  >
            </td>
            <td>
              <input type="number" name="pc"  >
            </td>
            <td>
              <input type="number" name="tf"  >
            </td>
            <td>
              <input type="number" name="pf"  >
            </td>


          </tr>
          @endforeach
        </tbody>
      </table>
      {{ $students->links() }}
    </div>
    @endif
  </div>
  </form>
</div>

@endsection

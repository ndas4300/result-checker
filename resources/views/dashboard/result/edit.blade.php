@extends('layouts.dashboard')

@section('content')

<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        Edit Result
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{ route('result.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        Back
      </a>
      <div class="btn-group">
        <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          Update Form
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <!--begin::Form-->
    <form class="form" id="kt_form" method="post" action="{{ route('result.update', $result->id) }}"
      enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          <div class="my-5">

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="student_id">Select Student<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="student_id" name="student_id">
                  @foreach ($students as $student)
                  <option value="{{ $student->id }}"   {{ ($result->$student->id == $student->id) ? 'selected' : null }}$student>
                    {{ $student->name }} ({{ $student->roll }})
                  </option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="semester_id">Select Semester<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="semester_id" name="semester_id">
                  @foreach ($semesters as $semester)
                  <option value="{{ $semester->id }}"  {{ ($result->semester->id == $semester->id) ? 'selected' : null }}>
                    {{ $semester->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="subject_id">Select Subject<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="subject_id" name="subject_id">
                  @foreach ($subjects as $subject)
                  <option value="{{ $subject->id }}"  {{ ($result->subject->id == $subject->id) ? 'selected' : null }}>
                    {{ $subject->name }} ({{ $subject->subject_code }})
                  </option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="tc">TC Number&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="tc" id="tc" value="{{ old('tc', $result->tc) }}"
                  class="form-control form-control-solid @error('tc') is-invalid @enderror" type="number" >
                @error('tc')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pc">PC Number&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="pc" id="pc" value="{{ old('pc', $result->pc) }}"
                  class="form-control form-control-solid @error('pc') is-invalid @enderror" type="number" >
                @error('pc')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="tf">TF Number&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="tf" id="tf" value="{{ old('tf', $result->tf) }}"
                  class="form-control form-control-solid @error('tf') is-invalid @enderror" type="number" >
                @error('tf')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="pf">PF Number&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="pf" id="pf" value="{{ old('pf', $result->pf) }}"
                  class="form-control form-control-solid @error('pf') is-invalid @enderror" type="number" >
                @error('pf')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>

          </div>
        </div>
        <div class="col-xl-2"></div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>

@endsection

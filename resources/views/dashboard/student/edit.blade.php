@extends('layouts.dashboard')

@section('content')

<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        Edit Student
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{ route('student.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        Back
      </a>
      <div class="btn-group">
        <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          Update Form
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <!--begin::Form-->
    <form class="form" id="kt_form" method="post" action="{{ route('student.update', $student->id) }}"
      enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          <div class="my-5">


            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="name" id="name" value="{{ old('name', $student->name) }}"
                  class="form-control form-control-solid @error('name') is-invalid @enderror" type="text" required>
                @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="roll">Roll &nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="roll" id="roll" value="{{ old('roll', $student->roll) }}"
                  class="form-control form-control-solid @error('roll') is-invalid @enderror" type="number" required>
                @error('roll')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="reg">Registration Num &nbsp;<span
                  class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="reg" id="reg" value="{{ old('reg', $student->reg) }}"
                  class="form-control form-control-solid @error('reg') is-invalid @enderror" type="number" required>
                @error('reg')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="session">Session &nbsp;<span
                  class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="session" id="session" value="{{ old('session', $student->session) }}"
                  class="form-control form-control-solid @error('session') is-invalid @enderror" type="text" required>
                @error('session')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="section_id">Select Section<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="section_id" name="section_id">
                  @foreach ($sections as $section)
                  <option value="{{ $section->id}}"  {{ ($result->section->id == $section->id) ? 'selected' : null }} >
                    {{ $section->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="shift_id">Select Shift<span class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="shift_id" name="shift_id">
                  @foreach ($shifts as $shift)
                  <option value="{{ $shift->id}}"   {{ ($result->shift->id == $shift->id) ? 'selected' : null }}>
                    {{ $shift->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="semester_id">Select Semester<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="semester_id" name="semester_id">
                  @foreach ($semesters as $semester)
                  <option value="{{ $semester->id }}"   {{ ($result->semester->id == $semester->id) ? 'selected' : null }} >
                    {{ $semester->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="department_id">Select Department<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="department_id" name="department_id">
                  @foreach ($departments as $department)
                  <option value="{{ $department->id}}"   {{  ($result->department->id == $department->id) ? 'selected' : null }} >
                    {{ $department->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>

          </div>
        </div>
        <div class="col-xl-2"></div>
      </div>
    </form>
    <!--end::Form-->
  </div>
</div>

@endsection

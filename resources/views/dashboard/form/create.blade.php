@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Add New Form
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('form.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
            <i class="ki ki-check icon-sm"></i>
            Save Form
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      <!--begin::Form-->
      <form class="form" id="kt_form" method="post" action="{{ route('form.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-xl-2"></div>
          <div class="col-xl-8">
            <div class="my-5">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="name" id="name" value="{{ old('name') }}" class="form-control form-control-solid @error('name') is-invalid @enderror" type="text" required>
                  @error('name')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="roll">Roll&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="roll" id="roll" value="{{ old('roll') }}" class="form-control form-control-solid @error('roll') is-invalid @enderror" type="number" required>
                  @error('roll')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="reg_no">Reg No.&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="reg_no" id="reg_no" value="{{ old('reg_no') }}" class="form-control form-control-solid @error('reg_no') is-invalid @enderror" type="number" required>
                  @error('reg_no')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="section">Section&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <select class="form-control select2" name="section" id="section">

                      <option value="M">M</option>
                      <option value="L">L</option>
                      <option value="XL">XL</option>
                      <option value="XXL">XXL</option>

                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="phone">Phone&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="phone" id="phone" value="{{ old('phone') }}" class="form-control form-control-solid @error('phone') is-invalid @enderror" type="number" required>
                  @error('phone')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="size">Size&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <select class="form-control select2" name="size" id="size">

                    <option value="A1">A1</option>
                    <option value="B1">B1</option>
                    <option value="A2">A2</option>
                    <option value="B2">B2</option>

                </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="religion">Religion&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="religion" id="religion" value="{{ old('religion') }}" class="form-control form-control-solid @error('religion') is-invalid @enderror" type="text" required>
                  @error('religion')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              {{-- <div class="form-group row">
                <label class="col-md-3 col-form-label" for="message">Message &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <textarea name="message" id="message" class="form-control form-control-solid @error('message') is-invalid @enderror" required>{{ old('message') }}</textarea>
                  @error('message')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div> --}}
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="status">Status</label>
                <div class="col-md-9">
                  <input type="hidden" checked value="0" name="status"/>
                  <span class="switch switch-icon">
                  <label>
                   <input type="checkbox" {{ old('status', 1) ? 'checked' : null }} value="1" name="status"/>
                   <span></span>
                  </label>
                 </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-2"></div>
        </div>
      </form>
      <!--end::Form-->
    </div>
  </div>

@endsection

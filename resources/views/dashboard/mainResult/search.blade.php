@extends('layouts.dashboard')

@section('content')

<div class="card card-custom card-sticky" id="kt_page_sticky_card">
  <div class="card-header">
    <div class="card-title">
      <h3 class="card-label">
        Search Result
      </h3>
    </div>
    <div class="card-toolbar">
      <a href="{{ route('student.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
        <i class="ki ki-long-arrow-back icon-sm"></i>
        Back
      </a>
      <div class="btn-group">
        <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
          <i class="ki ki-check icon-sm"></i>
          Save Form
        </button>
      </div>
    </div>
  </div>
  <div class="card-body">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <!--begin::Form-->
    <form class="form" id="kt_form" method="post" action="{{ route('mainResult.result') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8">
          <div class="my-5">


            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="department_id">Select Department<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="department_id" name="department_id">
                  @foreach ($departments as $department)
                    <option value="{{ $department->id }}">
                      {{ $department->name }}
                    </option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-lg-3 col-form-label" for="semester_id">Select Semester<span
                  class="text-danger">*</span></label>
              <div class="col-lg-9">
                <select class="form-control select2" id="semester_id" name="semester_id">
                  @foreach ($semesters as $semester)
                  <option value="{{ $semester->id }}">
                    {{ $semester->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>


            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="roll">Roll&nbsp;Num<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="roll" id="roll" value="{{ old('roll') }}"
                  class="form-control form-control-solid @error('roll') is-invalid @enderror" type="number" >
                @error('roll')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-form-label" for="reg">Resgistration Num&nbsp;<span class="text-danger">*</span></label>
              <div class="col-md-9">
                <input name="reg" id="reg" value="{{ old('reg') }}"
                  class="form-control form-control-solid @error('reg') is-invalid @enderror" type="number" >
                @error('reg')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>

          </div>
        </div>
        <div class="col-xl-2"></div>
      </div>
    </form>
  </div>
</div>

@endsection

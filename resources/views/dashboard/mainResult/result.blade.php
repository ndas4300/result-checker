@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
      <div class="card-title">
        <h3 class="card-label">Main Result List</h3>
      </div>
      <div class="card-toolbar">
        {{--                @can('web_content.create')--}}
        {{--                    <a href="{{ route('mainResult.create') }}" class="btn btn-primary font-weight-bolder">--}}
        {{--                        <span class="svg-icon svg-icon-md">--}}
        {{--                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
        {{--                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
        {{--                                    <rect x="0" y="0" width="24" height="24"></rect>--}}
        {{--                                    <circle fill="#000000" cx="9" cy="15" r="6"></circle>--}}
        {{--                                    <path--}}
        {{--                                        d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"--}}
        {{--                                        fill="#000000" opacity="0.3"></path>--}}
        {{--                                </g>--}}
        {{--                            </svg>--}}
        {{--                        </span>--}}
        {{--                        Add Result--}}
        {{--                    </a>--}}
        {{--                @endcan--}}
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-separate table-head-custom table-checkable" id="kt_datatable">
          <thead>
          <tr>
            <th>Name</th>
            <th>Roll</th>
            <th>Semester</th>
            <th>Grade</th>
            <th>Total Point</th>
            {{-- <th>Message</th> --}}
          </tr>
          </thead>
          <tbody>

            <tr>
              <td>
                {{ $mainResult->student->name }}
              </td>
              </td>
              <td>
                {{ $mainResult->student->roll }}
              </td>
              <td>
                {{ $mainResult->semester->name }}
              </td>
              <td>
                @if ( $mainResult->grade == "F" )

                  <span class="label label-danger label-pill label-inline mr-2">{{ $mainResult->grade }}</span>({{ $mainResult->fail }})

                @else
                  <span class="label label-rounded label-success mr-2">{{ $mainResult->grade }}</span>
                @endif
              </td>

              <td>
                {{ $mainResult->total_point }}
              </td>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

@endsection

@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Edit Subject
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('subject.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
            <i class="ki ki-check icon-sm"></i>
            Update Form
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      <!--begin::Form-->
      <form class="form" id="kt_form" method="post" action="{{ route('subject.update', $subject->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="row">
          <div class="col-xl-2"></div>
          <div class="col-xl-8">
            <div class="my-5">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="name" id="name" value="{{ old('name', $subject->name) }}"
                    class="form-control form-control-solid @error('name') is-invalid @enderror" type="text" required>
                  @error('name')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="semester_id">Select Semester<span
                    class="text-danger">*</span></label>
                <div class="col-lg-9">
                  <select class="form-control select2_multiple" id="semester_id" name="semester_id[]" multiple>
                    @foreach ($semesters as $semester)
                    <option value="{{ $semester->id }}"  {{ $subject->semesters->contains($semester->id) ? 'selected' : null }}>
                      {{ $semester->name }}
                    </option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-lg-3 col-form-label" for="department_id">Select Department<span
                    class="text-danger">*</span></label>
                <div class="col-lg-9">
                  <select class="form-control select2_multiple" id="department_id" name="department_id[]" multiple>
                    @foreach ($departments as $department)
                    <option value="{{ $department->id }}"   {{ $subject->departments->contains($department->id) ? 'selected' : null }}>
                      {{ $department->name }}
                    </option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="subject_code">Subject Code &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="subject_code" id="subject_code" value="{{ old('subject_code', $subject->subject_code) }}"
                         class="form-control form-control-solid @error('subject_code') is-invalid @enderror" type="number" required>
                  @error('subject_code')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>


              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="credit">Credit &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="credit" id="credit" value="{{ old('credit', $subject->credit) }}"
                    class="form-control form-control-solid @error('credit') is-invalid @enderror" type="number" required>
                  @error('credit')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>



              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tc">Total TC Marks &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tc" id="tc" value="{{ old('tc', $subject->tc) }}"
                         class="form-control form-control-solid @error('tc') is-invalid @enderror" type="number" >
                  @error('tc')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="pc">Total PC Marks &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="pc" id="pc" value="{{ old('pc', $subject->pc) }}"
                         class="form-control form-control-solid @error('pc') is-invalid @enderror" type="number" >
                  @error('pc')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="tf">Total TF Marks &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="tf" id="tf" value="{{ old('tf', $subject->tf) }}"
                         class="form-control form-control-solid @error('tf') is-invalid @enderror" type="number" >
                  @error('tf')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="pf">Total PF Marks &nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="pf" id="pf" value="{{ old('pf', $subject->pf) }}"
                         class="form-control form-control-solid @error('pf') is-invalid @enderror" type="number" >
                  @error('pf')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>

            </div>
          </div>
          <div class="col-xl-2"></div>
        </div>
      </form>
      <!--end::Form-->
    </div>
  </div>

@endsection

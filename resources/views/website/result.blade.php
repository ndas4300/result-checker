@extends('layouts.website')

@section('content')
  <section>
    <div class="content">
      <div class="container-fluid">
        <div class="row d-flex justify-content-center">
          <div class="col-md-6">

            <div class="card-body text-center">
              <img src="image/congratulation.gif" class="img-fluid" alt="">
              <!-- <img src="image/failed.gif" class="img-fluid" alt=""> -->
              <div class="header-are-center">
                @if(!isset($mainResult->total_point ) || $mainResult->total_point== 0 )
                  <h3> You Failed!</h3>
                @else
                  <h3>Congratulations, You Passed!</h3>
                @endif
              </div>
              <table class='table mt-5'>
                <tr>
                  <td>Your Roll : </td>
                  <td>{{$student->roll}}</td>
                </tr>
                <tr>
                  <td>You Got :</td>
                  <td>{{$mainResult->total_point}}</td>
                </tr>
                <tr>
                  <td>Semester :</td>
                  <td>{{$student->semester->name}}</td>
                </tr>
                <tr>
                  <td>Session :</td>
                  <td>{{$student->session}}</td>
                </tr>
              </table>
              <div class="text">


                <!-- <h1>Sorry, You Failed!</h1> -->

                <button class="btn btn-outline-primary"><a href="{{route('home')}}">search again</a></button>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section>
@endsection


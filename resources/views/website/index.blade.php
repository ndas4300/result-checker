@extends('layouts.website')

@section('content')
  <div class="form_part">
    <form class="form" id="kt_form" method="post" action="{{ route('result.search') }}">
      @csrf
      <table>
        <tr>
          <td> <label for="dep">Department</label> </td>
          <td>:</td>
          <td>
            <select name="department_id" id="exam">
              @foreach ($departments as $department)
                <option value="{{ $department->id }}">
                  {{ $department->name }}
                </option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td> <label for="sem">Semester</label> </td>
          <td>:</td>
          <td>
            <select name="semester_id" id="sem">
              @foreach ($semesters as $semester)
                <option value="{{ $semester->id }}">
                  {{ $semester->name }}
                </option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td> <label for="Roll">Roll</label> </td>
          <td>:</td>
          <td> <input id="Roll" name="roll" type="number"> </td>
        </tr>
        <tr>
          <td> <label for="Reg">Reg: No</label> </td>
          <td>:</td>
          <td> <input id="Reg" name="reg" type="number"> </td>
        </tr>
{{--        <tr>--}}
{{--          <td> <label for="num">4+2</label> </td>--}}
{{--          <td>=</td>--}}
{{--          <td> <input id="num" type="text"> </td>--}}
{{--        </tr>--}}
        <tr>
          <td> </td>
          <td></td>
          <td>
            <button>Reset</button>
            <button><a style="color: black;" type="submit_id">Submit</a></button>
          </td>
        </tr>
      </table>
    </form>
  </div>
@endsection
